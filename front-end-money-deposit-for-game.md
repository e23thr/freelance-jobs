# Scope
ทำเว็บ (Web frontend)) โดยใช้ React JS โดยใช้ template ของทางลูกค้า (https://themeforest.net/item/clean-ui-react-admin-template/21938700) 
โดยสามารถ reuse template ได้ทั้งหมดและมี API ให้เรียกใช้เป็นแบบ standard RESTful (ผู้ว่าจ้างได้จัดเตรียมไว้ให้)

## Sitemap/Features
1. Landing page ที่แสดง Article ทั่วไป (กรณีที่ยังไม่ login)
2. User registration มีข้อมูลการสมัครสมาชิกดังนี้ e-mail,password,confirm-password,ชื่อ-นามสกุล,เบอร์โทร,บัญชีสำหรับการเติมเงิน และหลังจากสมัครจะได้รับ token เข้าระบบ
3. User login โดยเมื่อ login ผ่าน จะได้รับ token ไว้ใช้กับ api ต่อๆ ไป
4. Home (หลังการ login) จะมีข้อมูลดังนี้ article ทั่วไป, navigation bar เมนูตาม sitemap นี้, section ส่วน user profile พร้อมเงินในเกมส์ที่เหลืออยู่ในระบบ
5. Profile มีฟังชั่นดังนี้ ข้อมูล user profile, แก้ไข (ข้อมูลบางอย่าง), deposit history, contact history, bonus gift usage history
6. Deposit มีการรับฝากเงินดังนี้ bank account, true wallet, truemoney โดยมี API ให้ดึงรายการและมีแบบฟอร์มการกรอกแต่ละแบบให้
7. Deposit contact center เป็น static page (content โดยผู้ว่าจ้าง)
8. Download Game เป็นหน้า static (content โดยผู้ว่าจ้าง)
9. Bonus code เป็นหน้าฟอร์มให้กรอกรหัสพิเศษ ซึ่งจะมี API ให้ตรวจสอบรหัส ก่อนทำการส่งข้อมูล
10. Contact us เป็น Static content โดยข้อมูลจะได้จากผู้ว่าจ้าง
11. Function Logout เพื่อออกจากระบบบ

## หมายเหตุ
** ระบบสมาชิกเป็นแบบ login ด้วย user/email + password เท่านั้น